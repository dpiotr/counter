/* Miernik czasu kierowcy ATMEGA8
 * @author pietr343
 * e-mail: dulpiotr@gmail.com
 * State:ALPHA VERSION
 * Date:07.07.2015
Zewnętrzny kwarc 8Mhz + 2x 22pF Fusebity : w lfuse 0 0xef
Beep=0 - no beep, 1- one beep, 2 - two beeps etc.
*/

#include <LiquidCrystal.h>
#include <EEPROM.h>


#define F_CPU 8000000UL   ///Tryb pracy 8MHz 
#define BRAKE 0
#define CHANGE_KEY 1
#define OK_KEY 2
#define ACC 3
#define LONGPRESS_LEN 1000
#define DELAY 20
#define BEEP_PIN 4


///VARIABLES
volatile boolean light=false;
volatile byte menu_timer=0;

volatile boolean button_flag[3][3]={false};  //[0]: true – nowy stan przycisku;[1]: false-krotki,true-dlugi;[2]: true – zignoruj nastepne wcisniecie
volatile boolean previous_state[3]={false}; 
volatile boolean current_state[3]={false}; 
volatile unsigned char button_pressed_counter[3]={0};//press duration
volatile byte screen_number=0;
volatile boolean mute=false;        ///EEPROM address 0
volatile boolean mode_auto=true;    ///EEPROM address 1

volatile boolean pause_15m=false;
volatile boolean pause_45m=false;

volatile byte seconds=0;
volatile byte minutes=0;
volatile byte hours=0;

volatile byte pause_seconds=0;
volatile byte pause_minutes=0;
volatile byte pause_hours=0;

volatile byte total_seconds=0;
volatile byte total_minutes=0;
volatile byte total_hours=0;

volatile byte counter=0;
volatile byte beep_count=0;


////FUNCTIONS
void setup();
void start_interrupts();
void reset_time();
bool reset_pause();
void drive();
void not_drive();
void total_drive();
void drive_time();
void pause_time();
void total_time();
void light_mesage();
void main_screen();
void mode_screen();
void sound_screen();
void pause_screen();
void reset_screen();
void beep();
void info_scren();
void loop();



LiquidCrystal lcd(9,10,8,7,6,5);    //RS,E,D4,D5,D6,D7


void start_timer_interrupts(){
  cli();
  /*
  Timer 0 - w Arduino funkcje delay;
  Timer 2 - w Arduino funkcje beep;
  */
  TCCR0 = _BV(CS02) | _BV(CS00);
  TIMSK= _BV(TOIE0);
  TCNT0=0;
  
  TCCR1A=0;
  TCCR1B=0;
  TCCR1B = _BV(CS12) ;   //Prescaler 256
  TIMSK =_BV(TOIE1);     //Set to OVERFLOW MODE
  TCNT1=64911;   // Przerwanie co 20ms
  
  sei();
}


void setup() {
  lcd.begin(16,2);    //Inicjalizacja lcd
  lcd.clear();
  lcd.print("INICJALIZACJA...");
  
  ////Wczytanie i ustawienie zmiennych
  mute=EEPROM.read(0);
  mode_auto=EEPROM.read(1);
  //mute=false;       TEST PURPOSES!!!
  if(!mode_auto) button_flag[BRAKE][2]=true; //Jeśli MANUAL to od razu włączona PAUZA 
  
  ///Ustawienie pinów
  pinMode(BRAKE,INPUT_PULLUP);
  pinMode(CHANGE_KEY,INPUT_PULLUP);
  pinMode(OK_KEY,INPUT_PULLUP);
  pinMode(BEEP_PIN,OUTPUT);
  
  Serial.begin(9600); //Komunikacja szeregowa
  //lcd.print("hello, world!");
  start_timer_interrupts(); //Włączenie obsługi przerwan dla timera 1(klawiatura)[50Hz] i 3(mierzenie czasu)[1Hz]
  lcd.setCursor(0,0);
  lcd.print(" Licznik  czasu ");
  lcd.setCursor(0,1);
  lcd.print(" Dul Piotr 2015 ");
  delay(3000);
  reset_times(); //Reset czasu
}

ISR(TIMER1_OVF_vect){
  TCNT1=64911;    //8MHz PRE256
  //TCNT1=65533;  //FOR TEST PURPOSES ONLY!!!!
  counter++;      //licznik sekund (jeśli 50 to jedna sekunda)

  if(counter>=50){
  total_drive();
    if(button_flag[BRAKE][2] || (mode_auto==true && light==false)){
      not_drive();
      if((hours==1 && minutes>=30 && pause_minutes>=15)
      || (hours==2 && pause_minutes>=15)
      || ( hours==3 && pause_minutes>=15)
      ||(hours==4 && minutes<30 && pause_minutes>=15)){
            if(pause_15m==false)
              beep_count=2;
            pause_15m=true;
            
          }
      
      
      if((hours>=4 && minutes>=30 && pause_15m==true && pause_minutes>=30)
          || (hours>=4 && minutes>=30 && pause_minutes>=45)){
            if(pause_45m==false)
              beep_count=3;
            pause_45m=true;
          }
    }  
    else{
      reset_pause();
      drive();
    }
    if(screen_number!=0){
      menu_timer++;
    }  
    if(menu_timer==5){
      screen_number=0;
      menu_timer=0;  
    }
    counter=0;
  }
  ///////End 
  
  if(mode_auto==true)
    if(digitalRead(ACC)==1)  light=true;
    else light=false;   
    
   /////Obsługa klawiatury
  for(int i=0;i<3;i++) {
  current_state[i] = !digitalRead(i); //logic reverse
  if(!current_state[i] && previous_state[i]){
    if((!button_flag[i][2])&&(button_pressed_counter[i]<(LONGPRESS_LEN/DELAY))){
      //flaga zignorowania nast. przycisku nie jest aktywna, a czas wcisniecia krotszy od czasu granicznego
      button_flag[i][0]=true;    //informacja ze jest nowy stan przycisku
      button_flag[i][1]=false;   //ten stan to krotkie wcisniecie
    }
  else{
    //jezeli należało zignorować to wciśnięcie przycisku
    button_flag[i][2]=false;//zignoruj je i wyczyść flagę
  }
 }
  if(current_state[i]){ //jeżeli przycisk jest cały czas wciśnięty
    button_pressed_counter[i]++;  //zwiększ licznik
    if((button_pressed_counter[i]>=(LONGPRESS_LEN/DELAY))&&(!button_flag[i][2])){//jeżeli mineło wystarczająco dużo czasu, żeby uznać to za długie wciśnięcie i jest to pierwsze wciśnięcie
      button_flag[i][0]=true;      //informacja że jest nowy stan przycisku
      button_flag[i][1]=true;      //ten stan to dlugie wcisniecie
      button_flag[i][2]=true;      //zignoruj kolejne wciśnięcia
      button_pressed_counter[i]=0; //wyczyść licznik
    }
  }
  else{
    button_pressed_counter[i]=0;
  }
 previous_state[i]=current_state[i];
 }
/////////////////////////////////////////
}

boolean reset_times(){
  seconds=0;
  minutes=0;
  hours=0;
  pause_seconds=0;
  pause_minutes=0;
  pause_hours=0;
  total_seconds=0;
  total_minutes=0;
  total_hours=0;
  pause_15m=false;
  pause_45m=false;
  return true;
}

boolean reset_pause(){
  pause_seconds=0;
  pause_minutes=0;
  pause_hours=0;
  return true;
}


void drive(){
  seconds++;
  if(seconds==60){
    minutes++;
    seconds=0;
  }
  if(minutes==60){
    hours++;
    minutes=0;
  }
  if(hours==12){
    hours=0;
    minutes=0;
    seconds=0;
    pause_15m=false;
    pause_45m=false;
  }
}

void not_drive(){
  pause_seconds++;
  if(pause_seconds==60){
    pause_minutes++;
    pause_seconds=0;
  }
  if(pause_minutes==60){
    pause_hours++;
    pause_minutes=0;
  }
  if(pause_hours==12){
    pause_hours=0;
    pause_minutes=0;
    pause_seconds=0;
    pause_15m=false;
    pause_45m=false;
  }
}

void total_drive(){
  total_seconds++;
  if(total_seconds==60){
    total_minutes++;
    total_seconds=0;
  }
  if(total_minutes==60){
    total_hours++;
    total_minutes=0;
  }
  if(total_hours==24){
    total_hours=0;
    total_minutes=0;
    total_seconds=0;
  }
}

void drive_time(){ 
  lcd.print("JAZDA:");
  if(hours<10) lcd.print("0");
  lcd.print(hours);
  lcd.print(":");
  if(minutes<10) lcd.print("0");
  lcd.print(minutes);
  lcd.print(":");
  if(seconds<10) lcd.print("0");
  lcd.print(seconds);
}
void pause_time(){   
  lcd.print("PAUZA:");
  if(pause_hours<10) lcd.print("0");
  lcd.print(pause_hours);
  lcd.print(":");
  if(pause_minutes<10) lcd.print("0");
  lcd.print(pause_minutes);
  lcd.print(":");
  if(pause_seconds<10) lcd.print("0");
  lcd.print(pause_seconds);
}

void total_time(){ 
  lcd.print("CALKO:");
  if(total_hours<10) lcd.print("0");
  lcd.print(total_hours);
  lcd.print(":");
  if(total_minutes<10) lcd.print("0");
  lcd.print(total_minutes);
  lcd.print(":");
  if(total_seconds<10) lcd.print("0");
  lcd.print(total_seconds);
}

void light_message(){
  lcd.setCursor(0,0);
  lcd.print(" WLACZ SWIATLA! ");
  lcd.setCursor(0,1);
  lcd.print(" ");
  pause_time();
  lcd.print(" ");
}

void main_screen(){
        if(mode_auto==true && light==false){
          light_message();
        if(button_flag[BRAKE][2])
          button_flag[BRAKE][2]=!button_flag[BRAKE][2];
          //delay(1000); 
        } else {
        lcd.setCursor(0,0);
        if(button_flag[BRAKE][2] || (mode_auto==true && light==false)){
          pause_time();
          if(pause_15m==true) lcd.print("*T");
          else lcd.print("*N");
          lcd.setCursor(0,1);
          drive_time();
          if(pause_45m==true) lcd.print("*T");
          else lcd.print("*N");
        }  
        else{
          drive_time();
          if(pause_15m==true) lcd.print("*T");
          else lcd.print("*N");
          lcd.setCursor(0,1);
          total_time();
          if(pause_45m==true) lcd.print("*T");
          else lcd.print("*N");    
        }
     }
 }

void sound_screen(){
  lcd.setCursor(0,0);
  lcd.print("BRZECZYK:");
  if(mute==false) lcd.print("WL ");
  else lcd.print("WYL ");
  lcd.setCursor(0,1);
  lcd.print("DALEJ>>");
  lcd.setCursor(11,1);
  lcd.print("ZMIEN");
  if(button_flag[OK_KEY][0]){
    if(button_flag[OK_KEY][1]){
      
    }
  else{
     mute=!mute;
     EEPROM.write(0,mute);
     menu_timer=0;    ///zerowanie czasu zamkniecia menu    
    }
    button_flag[OK_KEY][0]=false;
  }
}


void mode_screen(){
  lcd.setCursor(0,0);
  lcd.print("TRYB PRACY:");
  if(mode_auto==false) lcd.print("MANU");
  else lcd.print("AUTO");
  lcd.setCursor(0,1);
  lcd.print("DALEJ>>");
  lcd.setCursor(11,1);
  lcd.print("ZMIEN");
  
  if(button_flag[OK_KEY][0]){
    if(button_flag[OK_KEY][1]){
      //Serial.println("Dlugie");         
    }
  else{
     lcd.clear(); 
     mode_auto=!mode_auto;
     EEPROM.write(1,mode_auto);
     menu_timer=0;
  }
    button_flag[OK_KEY][0]=false;
  }
}  

void reset_screen(){
  lcd.setCursor(0,0);
  lcd.print("  RESET CZASU?");
  lcd.setCursor(0,1);
  lcd.print("DALEJ>>");
  lcd.setCursor(11,1);
  lcd.print("RESET");
  
  if(button_flag[OK_KEY][0]){
    if(button_flag[OK_KEY][1]){
     lcd.setCursor(0,0); 
     lcd.clear(); 
     if(reset_times()){
        lcd.print("  ZRESETOWANO!");
        delay(30000);
        lcd.clear(); 
        screen_number=0;
        menu_timer=0;
     }
     else{
      lcd.print("BLAD!");
      delay(3000);
      lcd.clear(); 
      screen_number=0;
     }      
    }
  else{
    }
    button_flag[OK_KEY][0]=false;
  }
 }  

void pause_screen(){
  lcd.setCursor(0,0);
  lcd.print("PAUZA 15M:");
  if(pause_15m==false) lcd.print("NIE");
  else lcd.print("TAK");
  lcd.setCursor(0,1);
  lcd.print("PAUZA 30/45M:");
  if(pause_45m==false) lcd.print("NIE");
  else lcd.print("TAK");
}

void beep(int count){

  for(int i=0;i<count;i++){
    digitalWrite(BEEP_PIN,HIGH);
    delay(3000);
    digitalWrite(BEEP_PIN,LOW);
    delay(3000);
  }

}

void info_screen(){
  lcd.setCursor(0,0);
  lcd.print("INFO:dulpiotr");
  lcd.setCursor(0,1);
  lcd.print("     @gmail.com");
}

void loop(){
  
  if(button_flag[CHANGE_KEY][0]){
    if(button_flag[CHANGE_KEY][1]){         
    }
    else{
      if(screen_number>=5){
        lcd.clear();
        screen_number=0;
      }
      else{
        lcd.clear();
        screen_number++;
        menu_timer=0;
        }
      }
      button_flag[CHANGE_KEY][0]=false;
  }
    
  if(button_flag[OK_KEY][0] && screen_number==0 ){
    if(button_flag[OK_KEY][1]){
    }
    else{
      button_flag[BRAKE][2]=!button_flag[BRAKE][2];
    }
    button_flag[OK_KEY][0]=false;
 }

 if(mute==false && beep_count!=0){
  beep(beep_count);
  beep_count=0;
 }

 
  switch(screen_number){
    case 0:
    main_screen();
    break;
    case 1:
    sound_screen();
    break;
    case 2:
    mode_screen();
    break;
    case 3:
    reset_screen();
    break;
    case 4:
    pause_screen();
    break;
    case 5:
    info_screen();
    break;
    default:
    main_screen();
    break;
  }

}
